#include "makefile_modifiers.hpp"

using namespace std;

static map<string, string> linking;

void create_link_option(const std::string& file, const std::string& link_flags)
{
	linking[file]=link_flags;
}

bool is_library_header(const std::string& path)
{
	return (linking.find(get_file_without_directory(path)) != linking.end());
}

std::string get_link_flags(const std::string& path)
{
	string file_to_link = get_file_without_directory(path);
	auto it = linking.find(file_to_link);

	if(it == linking.end())
	{
		return "";
	}

	return it->second;
}

std::string get_link_pgk_config(const std::string& lib)
{
	return " $(shell pkg-config --libs " + lib + " ) ";
}

