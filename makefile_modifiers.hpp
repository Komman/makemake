#ifndef _MAKEFILE_MODIFIERS_HPP_
#define _MAKEFILE_MODIFIERS_HPP_

#include "utils.hpp"

void create_link_option(const std::string& file, const std::string& link_flags);

bool is_library_header(const std::string& path);
std::string get_link_flags(const std::string& file);

std::string get_link_pgk_config(const std::string& lib);

#endif //_MAKEFILE_MODIFIERS_HPP_
