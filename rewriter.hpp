#ifndef _REWRITER_HPP_
#define _REWRITER_HPP_

#include "linked_library.hpp"
#include "utils.hpp"

const inline std::string MAKEMAKE_TMP_PATH = "makemake_tmp";
const inline std::string NEW_INCLUDE_TOKEN = "$makemake_include";


class Rewriter
{
public:
	Rewriter(const std::string& file_path);

	bool is_main() const;

	const std::vector<std::string>&    get_includes()     const;
	const std::vector<std::string>&    get_env_includes() const;
	const std::vector<flagTypes>&      get_flags()        const;
	extTypes                           get_ext()          const;
	const std::vector<LinkedLibrary*>& get_libs()         const;

private:
	const std::string _file_path;

	std::vector<std::string> _includes;
	std::vector<std::string> _env_includes;
	std::vector<flagTypes>   _flags;
	extTypes                 _ext;
	std::vector<LinkedLibrary*>  _libs;

	bool _is_main;
};

#endif //_REWRITER_HPP_
