#ifndef _LINKED_LIBRARY_HPP_
#define _LINKED_LIBRARY_HPP_

#include <string>

class LinkedLibrary
{
public:

	virtual std::string name() const =0;
	virtual std::string include_path() const =0;
	
protected:

private:

};

#endif //_LINKED_LIBRARY_HPP_
