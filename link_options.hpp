#ifndef _LINK_OPTIONS_HPP_
#define _LINK_OPTIONS_HPP_

#include "makefile_modifiers.hpp"
#include "syntax.hpp"

inline void build_all_library()
{
	create_link_option("pthread.h", "-lpthread");
	create_link_option("thread", "-lpthread");
	create_link_option("ao.h", "-lao -ldl -lm");
	create_link_option("glew.h",  get_link_pgk_config("glfw3") 
								+ get_link_pgk_config("glew")
								+ get_link_pgk_config("x11")
								+ get_link_pgk_config("gl")
								+ "-lpthread -ldl");
	create_link_option("ssl.hpp", "-L=" + SSL_PATH + " -lssl"
								 + get_link_pgk_config("glfw3") 
		                    	 + get_link_pgk_config("glew")
		                    	 + get_link_pgk_config("x11")
		                    	 + get_link_pgk_config("gl")
		                    	 + "-lpthread -ldl");
}

#endif //_LINK_OPTIONS_HPP_
