#ifndef _PARSER_FILE_HPP_
#define _PARSER_FILE_HPP_

#include "utils.hpp"
#include "syntax.hpp"

class ParsedFile
{
public:
	ParsedFile();
	ParsedFile(const std::string& path);

	void compute_includes(const std::map<std::string, ParsedFile>& tomakefiles);
	const std::vector<ParsedFile*>& get_recursive_includes(const std::map<std::string, ParsedFile>& tomakefiles);

	const fileAdapter& get_adapted();
	const std::vector<std::string>& get_library_headers();

	void show();
	
	void show_includes();
	bool is_main();
	bool is_static_lib();
	bool to_ignore();

private:

	fileAdapter _adapted;
	std::vector<fileAdapter> _includes;
	bool                     _is_main;
	bool                     _to_ignore;
	bool                     _static_lib;
	std::vector<std::string> _library_headers;
	
	std::vector<ParsedFile*> _includes_ptr;

};

std::map<std::string, ParsedFile> get_unlinked_parsed_files(const std::vector<fileAdapter>& new_files_name);


#endif //_PARSER_FILE_HPP_
