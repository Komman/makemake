#ifndef _GENERATOR_HPP_
#define _GENERATOR_HPP_

#include <map>

#include "utils.hpp"
#include "parser_file.hpp"


void generate_makefile(std::map<std::string, ParsedFile>& tomakefiles,
					   const std::vector<std::string>& args);


#endif //_GENERATOR_HPP_
