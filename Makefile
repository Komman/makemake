CC=g++
WFLAGS=-Wall -march=native -O0 -g -std=c++17
LDFLAGS=-lpthread
EXE=run

all : code

obj:
	if [ ! -d obj ]; then mkdir obj;fi

install: code
	sudo mv run /usr/bin/makemake

run: code
	./${EXE}

clean:
	rm -rf obj
	rm -rf ${EXE}

obj/generator.o: generator.cpp generator.hpp makefile_modifiers.hpp utils.hpp parser_file.hpp syntax.hpp 
	${CC} ${WFLAGS} -c $< -o $@

obj/makefile_modifiers.o: makefile_modifiers.cpp makefile_modifiers.hpp utils.hpp 
	${CC} ${WFLAGS} -c $< -o $@

obj/makemake.o: makemake.cpp parser_file.hpp makefile_modifiers.hpp link_options.hpp generator.hpp utils.hpp syntax.hpp 
	${CC} ${WFLAGS} -c $< -o $@

obj/parser_file.o: parser_file.cpp parser_file.hpp makefile_modifiers.hpp utils.hpp syntax.hpp 
	${CC} ${WFLAGS} -c $< -o $@

obj/rewriter.o: rewriter.cpp rewriter.hpp linked_library.hpp utils.hpp 
	${CC} ${WFLAGS} -c $< -o $@

obj/utils.o: utils.cpp utils.hpp rewriter.hpp syntax.hpp linked_library.hpp 
	${CC} ${WFLAGS} -c $< -o $@

code: obj obj/generator.o obj/makefile_modifiers.o obj/makemake.o obj/parser_file.o obj/rewriter.o obj/utils.o 
	${CC} ${WFLAGS} obj/generator.o obj/makefile_modifiers.o obj/makemake.o obj/parser_file.o obj/rewriter.o obj/utils.o -o ${EXE} ${LDFLAGS}
