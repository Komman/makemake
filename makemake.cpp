#include <iostream>

#include "parser_file.hpp"
#include "makefile_modifiers.hpp"
#include "link_options.hpp"
#include "generator.hpp"

#include <filesystem>
#include <map>
#include <thread>

using namespace std;


int main(int argc, char* argv[])
{
	system("make clean -s");

	build_all_library();

	vector<string> all_files = get_all_files();	
	vector<fileAdapter> new_files_name = transform_path(all_files);
	map<string, ParsedFile> tomakefiles = get_unlinked_parsed_files(new_files_name);

	for(auto& a : tomakefiles)
	{
		a.second.compute_includes(tomakefiles);
	}

	vector<string> argvstr;
	for(int i=0;i<argc; i++)
	{
		argvstr.push_back(string(argv[i]));
	}

	generate_makefile(tomakefiles, argvstr);

	cout<<"Makfile generated !"<<endl;

	return 0;
}

/*
	std::filesystem::remove_all(MAKEMAKE_TMP_PATH);
	std::filesystem::remove_all("../"+MAKEMAKE_TMP_PATH);

	if(!std::filesystem::create_directory("../" + MAKEMAKE_TMP_PATH))
		err("Can't create ../" + MAKEMAKE_TMP_PATH);

	system(("cp -r . ../" + MAKEMAKE_TMP_PATH + "/").c_str());
	system(("mv ../" + MAKEMAKE_TMP_PATH + " ./").c_str());
	std::filesystem::remove_all("../"+MAKEMAKE_TMP_PATH);

	std::vector<ParsedFile> all_infos;
	for(auto& a : new_files_name)
	{
		// cout<<a.path<<": "<<a.adapted_path<<" ("<<get_str_from_ext(a.ext)<<")"<<endl;
		if(a.ext != NO_EXT)
		{
			all_infos.push_back({a, Rewriter(a.path)});

			if(all_infos[all_infos.size()-1].file.is_main())
			{
				cout<<all_infos[all_infos.size()-1].adapted.path<<endl;
			}
		}
	}

	std::filesystem::remove_all(MAKEMAKE_TMP_PATH);
*/