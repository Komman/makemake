#ifndef _SYNTAX_HPP_
#define _SYNTAX_HPP_

#include <string>

const inline std::string MAKEMAKE_STATIC_LIBRARY_TOKEN = "//MAKEMAKE_STATIC_LIBRARY";
const inline std::string MAKEMAKE_FILE_TO_IGNORE_TOKEN = "//MAKEMAKE_IGNORE";

const inline std::string SSL_PATH = "/home/comment/tout/programmation/openGL/ssl/ssl";
const inline std::string DEBUG_PERF_FLAGS = "-O0 -g";
const inline std::string DEFAULT_PERF_FLAGS = DEBUG_PERF_FLAGS;
const inline std::string OPTIM_PERF_FLAGS = "-O3";

#endif //_SYNTAX_HPP_
