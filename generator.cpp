#include "generator.hpp"
#include "makefile_modifiers.hpp"
#include "syntax.hpp"

#include <fstream>
#include <set>

using namespace std;

struct projectInfo
{
	string compiler;
	string add_flags;
	string static_lib_name; //"" for no static lib
};

struct argsModifiers
{
	string ldflags;
	string wflags;
	string rules;
	string new_default_rule;//"" for default
};

static projectInfo get_project_info(map<string, ParsedFile>& tomakefiles)
{
	projectInfo ret;
	ret.static_lib_name = "";

	for(auto& a : tomakefiles)
	{
		if(a.second.get_adapted().ext == CPP_EXT || a.second.get_adapted().ext == HPP_EXT)
		{
			ret.compiler = "g++";
			ret.add_flags = "-std=c++17";
		}

		if(a.second.is_static_lib())
		{
			ret.static_lib_name = transform_path(get_file_without_directory(a.second.get_adapted().path)).adapted_path;
		}
	}

	if(ret.compiler == "")
	{
		ret.compiler = "gcc";
	}

	return ret;
}

static string get_all_link_flags(map<string, ParsedFile>& tomakefiles)
{
	std::set<string> links_flags_set;

	for(auto& a : tomakefiles)
	{
		const auto& libs = a.second.get_library_headers();

		for(const auto& lib : libs)
		{
			links_flags_set.insert(get_link_flags(lib));
		}
	}

	string links_flags;
	for(const auto& flag : links_flags_set)
	{
		links_flags += flag;
		links_flags += ' ';
	}

	return links_flags;
}

static string get_ldflags_from_args(const std::vector<std::string>& args)
{
	string ret;
	for(const auto& arg : args)
	{
		if(arg.size()>0 && arg[0]=='-')
		{
			ret+=arg+" ";
		}
	}
	return ret;
}

static argsModifiers parse_args(const std::vector<std::string>& args)
{
	argsModifiers ret;

	ret.ldflags = get_ldflags_from_args(args);
	ret.wflags   = DEFAULT_PERF_FLAGS;

	for(const auto& arg : args)
	{
		if(arg.size()>0 && arg[0]!='-')
		{
			if(arg == "ssl")
			{	
				ret.rules += "ssl:\n\tcd " + SSL_PATH + "; make -j31; cd -\n\tmake code -j31\n";
			}

			if(arg == "perf")
			{	
				ret.wflags = OPTIM_PERF_FLAGS;
			}

			if(arg == "debug")
			{	
				ret.wflags = DEBUG_PERF_FLAGS;
			}
		}
	}

	return ret;
}

static void write_makefile(map<string, ParsedFile>& tomakefiles, ofstream& makefile, const std::vector<std::string>& args)
{
	projectInfo   infos = get_project_info(tomakefiles);
	argsModifiers argparsed = parse_args(args);

	if(argparsed.new_default_rule == "")
	{
		argparsed.new_default_rule = "code";
	}

	makefile << "CC=" << infos.compiler << endl;
	makefile << "WFLAGS=-Wall -march=native " << argparsed.wflags << " " << infos.add_flags;
	makefile << endl;
	makefile << "LDFLAGS="<< argparsed.ldflags << " " << get_all_link_flags(tomakefiles) << endl;
	makefile << "EXE=run"<<endl;
	makefile << endl;
	makefile << "all :" << argparsed.new_default_rule << endl;
	makefile << endl;
	makefile << "obj:" << endl;
	makefile << "\tif [ ! -d obj ]; then mkdir obj;fi" << endl;
	makefile << endl;
	makefile << "run: code" << endl;
	makefile << "\t./${EXE}" << endl;
	makefile << endl;
	makefile << "clean:" << endl;
	makefile << "\trm -rf obj" << endl;
	makefile << "\trm -rf ${EXE}" << endl;
	makefile << endl;

	vector<string> obj_rules;
	for(auto& codefile : tomakefiles)
	{
		const auto& adapted = codefile.second.get_adapted();

		if(adapted.ext == C_EXT || adapted.ext == CPP_EXT)
		{
			obj_rules.push_back("obj/" + adapted.adapted_path + ".o");
			makefile << obj_rules[obj_rules.size()-1] << ": " << adapted.path << " ";

			auto& code_includes = codefile.second.get_recursive_includes(tomakefiles);
			for(auto& include : code_includes)
			{
				makefile << include->get_adapted().path << " ";
			}
			makefile << endl;
			
			makefile << "\t${CC} ${WFLAGS} -c $< -o $@" << endl;
			makefile << endl;
		}		
	}

	makefile<<argparsed.rules<<endl;

	makefile << "nolink: obj ";
	for(auto& obj_rule : obj_rules)
	{
		makefile << obj_rule << " ";
	}
	makefile<<endl<<endl;


	makefile << "code: obj ";
	for(auto& obj_rule : obj_rules)
	{
		makefile << obj_rule << " ";
	}
	makefile<<endl;

	if(infos.static_lib_name == "")
	{
		makefile<<"\t${CC} ${WFLAGS} ";
		for(auto& obj_rule : obj_rules)
		{
			makefile << obj_rule << " ";
		}
		makefile<<"-o ${EXE} ${LDFLAGS}"<<endl;
	}
	else
	{
		string libname = "lib" + infos.static_lib_name + ".a";
		makefile << "\trm -f "<<libname<<";ar rvs "<<libname<<" ";
		for(auto& obj_rule : obj_rules)
		{
			makefile << obj_rule << " ";
		}
		makefile << "> ar_logs.txt" <<endl;
	}
	
}

void generate_makefile(map<string, ParsedFile>& tomakefiles, const std::vector<std::string>& args)
{
	// for(auto& a : tomakefiles)
	// {
	// 	a.second.get_recursive_includes(tomakefiles);
	// 	a.second.show();
	// }

	ofstream makefile("Makefile");

	if(!makefile.is_open())
	{
		err("Failed to create Makefile");
	}

	write_makefile(tomakefiles, makefile, args);

	makefile.close();
}
