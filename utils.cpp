#include "utils.hpp"
#include "rewriter.hpp"
#include "syntax.hpp"


#include <map>
#include <unistd.h>

using namespace std;

const inline std::string MAKEMAKE_ALL_FILES = "makemake_tmp_all_files.txt";


const std::map<std::string, extTypes> EXTS={
	{"c", C_EXT},
	{"h", H_EXT},
	{"cpp", CPP_EXT},
	{"hpp", HPP_EXT}
};

const std::string EXTS_STR[EXT_COUNT] = {
	NO_EXT_STR,
	"c",
	"h",
	"cpp",
	"hpp"
};

extTypes    get_ext_from_str(const std::string& ext)
{
	auto it = EXTS.find(ext);

	if(it == EXTS.end())
		return NO_EXT;

	return it->second;
}

std::string get_str_from_ext(extTypes ext)
{
	return EXTS_STR[ext];
}

string get_file_ext_str(const std::string& file_path)
{
	string extstr_inv;
	string extstr;

	for(int i=file_path.size()-1; i>=0; i--)
	{
		if(file_path[i] == '.')
			break;

		extstr_inv+=file_path[i];
	}

	for(int i=extstr_inv.size()-1; i>=0; i--)
	{
		extstr+=extstr_inv[i];
	}

	return extstr;
}

extTypes get_file_ext(const std::string& file_path)
{
	return get_ext_from_str(get_file_ext_str(file_path));
}



std::vector<std::string> get_all_files()
{
	system(("find . > " + MAKEMAKE_ALL_FILES).c_str());
	
	std::vector<std::string> ret;
	
	ifstream file(MAKEMAKE_ALL_FILES);
	if(!file.is_open())
	{
		err("failed to open " + MAKEMAKE_ALL_FILES);
	}

	string line;
	while(getline(file, line))
	{
		ret.push_back(line);
	}

	system(("rm -f " + MAKEMAKE_ALL_FILES).c_str());
	return ret;
}

bool is_in_str(const std::string& src, unsigned int index, const std::string& word)
{
	if(index >= src.size())
		return false;
	if(src.size()-index < word.size())
		return false;

	unsigned int i=0;
	while(i<word.size() && src[index+i] == word[i])
	{
		i++;
	}

	return (i == word.size());
}

void includeInfo::show()
{
	std::cout<<"file: "<<file<<std::endl;
	std::cout<<"type: ";
	if(type == NO_INCLUDE) std::cout<<"NO_INCLUDE";
	if(type == FROM_ENV)   std::cout<<"FROM_ENV";
	if(type == FROM_FILE)  std::cout<<"FROM_FILE";
	std::cout<<std::endl;
}


void fileAdapter::show()
{
	std::cout<<"path: "<<path<<std::endl;
	std::cout<<"adapted_path: "<<adapted_path<<std::endl;
	std::cout<<"ext: "<< get_str_from_ext(ext) <<endl;
	std::cout<<std::endl;
}

includeInfo get_included_file(const std::string& line)
{
	unsigned int i=0;
	while(i<line.size() && line[i]!=MACRO_TOKEN[0] && (line[i]==' ' || line[i]=='\t'))
	{
		i++;
	}

	if(i == line.size())
	{
		return {"", NO_INCLUDE};
	}

	if(!is_in_str(line, i, INCLUDE_KEY))
	{
		return {"", NO_INCLUDE};
	}
	i+=INCLUDE_KEY.size();

	if(i>=line.size() || !(line[i]==' ' || line[i]=='\t'))
	{
		return {"", NO_INCLUDE};
	}

	while(i<line.size() && (line[i]==' ' || line[i]=='\t'))
	{
		i++;
	}

	includeInfo ret = {"", NO_INCLUDE};

	if(line[i] == '<')
	{
		ret.type = FROM_ENV;
	}
	if(line[i] == '\"')
	{
		ret.type = FROM_FILE;
	}
	if(ret.type == NO_INCLUDE)
	{
		return ret;
	}

	ret.file.reserve(line.size()-i);
	i++;

	while(i<line.size() && line[i]!='>' && line[i]!='\"')
	{
		ret.file.push_back(line[i]);
		i++;
	}

	if(line[i] != '>' && line[i] != '\"')
	{
		return {"", NO_INCLUDE};
	}

	return ret;
}


std::string              get_directory(const std::string& path)
{
	string dir;
	string pot_dir;

	for(auto c : path)
	{
		pot_dir+=c;
		if(c=='/')
		{
			dir+=pot_dir;
			pot_dir="";
		}
	}

	return dir;
}

std::string              get_file_without_directory(const std::string& path)
{
	string file;

	for(auto c : path)
	{
		file+=c;
		if(c=='/')
		{
			file="";
		}
	}

	return file;
}

bool to_make_file(extTypes ext)
{
	return (ext == C_EXT || ext == H_EXT || ext == CPP_EXT || ext == HPP_EXT);
}

std::vector<std::string> get_interesting_lines(const std::string& path)
{
	#ifdef VERBOSE
	cout<<"Building rule for "<<path<<" ...";
	cout.flush();
	#endif

	#ifdef VERBOSE_DOTS
	cout<<".";
	cout.flush();
	#endif

	string command = "cat " + path + " | grep '#include[\\ \\t][\\ \\t]*[\\\"<]\\|int[\\ \\t][\\ \\t]*main[\\ \\t]*(.*)\\|" 
					+ MAKEMAKE_STATIC_LIBRARY_TOKEN + "\\|" + MAKEMAKE_FILE_TO_IGNORE_TOKEN + "'";
	// cout<<command<<endl;
	
	std::vector<std::string> ret = system_result(command);
	
	return ret;	
}

bool                     insteresting_is_main(const std::string& interesting)
{
	unsigned int i;
	for(i=0; i<interesting.size();i++)
	{
		if(is_in_str(interesting, i, PREMAIN_KEY))
		{
			i+=PREMAIN_KEY.size();
			break;
		}
	}	

	if(i >= interesting.size())
		return false;

	while(i<interesting.size() && (interesting[i] == ' ' || interesting[i] == '\t'))
	{
		i++;
	}

	if(!is_in_str(interesting, i, MAIN_KEY))
		return false;

	i+=MAIN_KEY.size();

	while(i<interesting.size() && (interesting[i] == ' ' || interesting[i] == '\t'))
	{
		i++;
	}

	if(interesting[i] != '(')
		return false;

	return true;	
}

string sed_string(const std::string& str, char src, char dst)
{
	string ret = str;
	for(unsigned int i=0; i<ret.size();i++)
	{
		if(ret[i] == src)
		{
			ret[i] = dst;
		}
	}
	return ret;
}

fileAdapter              transform_path(const std::string& p)
{
	fileAdapter adapted;
	adapted.ext = get_file_ext(p);

	std::vector<std::string> interm({""});
	for(unsigned int i=0;i<p.size();i++)
	{
		if(p[i] != '/')
		{
			if(interm[interm.size()-1].size()==0)
			{
				if(p[i] == '.' && i+1<p.size() && p[i+1] == '.')
				{
					if(interm.size()<=1)
					{
						//err("Path that begins by \"..\"");
						adapted.adapted_path = INCLUDE_FILE_OUT_OF_DIRECTORY;
						adapted.path = INCLUDE_FILE_OUT_OF_DIRECTORY;
						return adapted;
					}
					interm.pop_back();
					interm.pop_back();
					interm.push_back("");
					i+=2;
					continue;
				}
				if(p[i] == '.' && i+1<p.size() && p[i+1] == '/')
				{
					i++;
					continue;
				}
			}
			interm[interm.size()-1]+=p[i];
		}
		else
		{
			interm.push_back("");
		}
	}

	std::string new_name = interm[0];
	std::string path     = interm[0];
	for(unsigned int i=1;i<interm.size();i++)
	{
		new_name+=ADAPTED_PATH_DIR_MARK;
		new_name+=interm[i];
		path+='/';
		path+=interm[i];
	}

	if(adapted.ext != NO_EXT && new_name.size()>0)
	{
		while(new_name.size()>0 && new_name[new_name.size()-1]!='.')
		{
			new_name.pop_back();
		}
		if(new_name.size()>0)
		{
			new_name.pop_back();
		}
	}

	adapted.adapted_path = new_name;
	adapted.path = path;

	return adapted;
}

std::vector<fileAdapter> transform_path(const std::vector<std::string>& paths)
{
	std::vector<fileAdapter> ret;

	for(const auto& p : paths)
	{
		ret.push_back(transform_path(p));
	}	

	return ret;
}

const static string system_result_tmp_file = "makemake_tmp_output_sysres.txt";

std::vector<std::string> system_result(const std::string& shell_command)
{
	system((shell_command + " > " + system_result_tmp_file).c_str());

	string line;
	ifstream file(system_result_tmp_file);
	std::vector<std::string> ret;

	while(getline(file, line))
	{
		ret.push_back(line);
	}

	file.close();

	system(("rm " + system_result_tmp_file).c_str());

	return ret;
}
