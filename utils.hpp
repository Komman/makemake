#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <vector>
#include <string>
#include <memory>
#include <iostream>
#include <fstream>
#include <map>


// #define VERBOSE
// #define VERBOSE_DOTS

#define PRINT_IGONRE
#define PRINT_STATIC_LIBRARY


#define BOUH std::cout<<"BOUH"<<std::endl;

inline void err(const std::string& msg)
{
	std::cout<<"makemake error: "<<msg<<std::endl;
	exit(-1);
}

const inline std::string INCLUDE_KEY = "#include";
const inline std::string MACRO_TOKEN = "#";
const inline std::string NO_EXT_STR  = "none";
const inline std::string PREMAIN_KEY = "int";
const inline std::string MAIN_KEY    = "main";

constexpr char ADAPTED_PATH_DIR_MARK  = '_';
const inline std::string INCLUDE_FILE_OUT_OF_DIRECTORY  = "error";

enum flagTypes {NO_FLAG, MAIN, STATIC_LIBRARY};
enum extTypes  {NO_EXT, C_EXT, H_EXT, CPP_EXT, HPP_EXT,
				EXT_COUNT};
enum includeType {NO_INCLUDE=0, FROM_ENV, FROM_FILE};

struct includeInfo
{
	std::string file;
	includeType type;

	void show();
};
struct fileAdapter
{
	std::string path;
	std::string adapted_path;
	extTypes    ext;
	
	void show();
};


std::string get_file_ext_str(const std::string& file_path);
extTypes    get_file_ext(const std::string& file_path);
extTypes    get_ext_from_str(const std::string& ext);
std::string get_str_from_ext(extTypes ext);

std::string              get_directory(const std::string& path);
std::string              get_file_without_directory(const std::string& path);
std::vector<std::string> get_all_files();
bool                     is_in_str(const std::string& src, unsigned int index, const std::string& word);
includeInfo              get_included_file(const std::string& line);
std::vector<fileAdapter> transform_path(const std::vector<std::string>& paths);
fileAdapter              transform_path(const std::string& path);

std::vector<std::string> system_result(const std::string& shell_command);
bool                     to_make_file(extTypes ext);
std::vector<std::string> get_interesting_lines(const std::string& path);
bool                     insteresting_is_main(const std::string& interesting);

template<typename T>
void print_vector(const std::vector<T>& v)
{
	std::cout<<"{ ";
	for(const auto& e : v)
	{
		std::cout<<e<<", ";
	}
	std::cout<<"}"<<std::endl;
}

namespace TERM
{
	const std::string BLACK  = "\033[1;30m";
	const std::string RED    = "\033[1;31m";
	const std::string GREEN  = "\033[1;32m";
	const std::string ORANGE = "\033[1;33m";
	const std::string BLUE   = "\033[1;34m";
	const std::string PURPLE = "\033[1;35m";
	const std::string CYAN   = "\033[1;36m";
	const std::string WHITE  = "\033[1;37m";
	const std::string NOCOL  = "\033[0m";
};

#endif //_UTILS_HPP_
