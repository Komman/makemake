#include "parser_file.hpp"

// #include <GL/glew.h>
#include "makefile_modifiers.hpp"

using namespace std;

ParsedFile::ParsedFile()
{

}

ParsedFile::ParsedFile(const std::string& path)
	: _adapted(transform_path(path))
{
	vector<string> interestings = get_interesting_lines(path);
	_is_main = false;
	_to_ignore = false;
	_static_lib = false;

	for(auto& interest : interestings)
	{
		includeInfo incls = get_included_file(interest);

		if(incls.type == FROM_FILE || incls.type == FROM_ENV)
		{
			if(is_library_header(incls.file))
			{
				_library_headers.push_back(incls.file);
			}
		}

		if(incls.type == FROM_FILE)
		{
			string abs_include = get_directory(path) + incls.file;
			auto to_push = transform_path(abs_include);

			if(to_push.path !=INCLUDE_FILE_OUT_OF_DIRECTORY)
			{
				_includes.push_back(to_push);
			}
		}
		else if(incls.type == FROM_ENV)
		{
			
		}
		else if(insteresting_is_main(interest))
		{
			_is_main = true;
		}
		else if(interest == MAKEMAKE_FILE_TO_IGNORE_TOKEN)
		{
			#ifdef PRINT_IGONRE
			cout<<"File ignored: "<<path<<endl;
			#endif
			
			_to_ignore = true;
			break;
		}
		else if(interest == MAKEMAKE_STATIC_LIBRARY_TOKEN)
		{
			#ifdef PRINT_STATIC_LIBRARY
			cout<<"Static library in: "<<path<<endl;
			#endif
			
			_static_lib = true;
		}
	}
}

void ParsedFile::show()
{
	cout<<""<<_adapted.path<<":"<<endl;
	cout<<"\tincludes : {";
	for(auto& incl : _includes_ptr)
	{
		cout<<incl->_adapted.path<<" ";
	}
	cout<<"}"<<endl;
	cout<<"\tlibrary headers : {";
	for(auto& header : _library_headers)
	{
		cout<<header<<" ";
	}
	cout<<"}"<<endl;
	cout<<"\tis_main : "<<_is_main<<endl;
	cout<<"\tto_ignore: "<<_to_ignore<<endl;
	cout<<"\tstatic_lib: "<<_static_lib<<endl;
}

void ParsedFile::show_includes()
{
	cout<<" = "<<_adapted.path<<" = "<<endl;
	for(auto& in : _includes)
	{
		in.show();
	}
}

const std::vector<std::string>& ParsedFile::get_library_headers()
{
	return _library_headers;
}

bool ParsedFile::is_main()
{
	return _is_main;
}
bool ParsedFile::is_static_lib()
{
	return _static_lib;
}
bool ParsedFile::to_ignore()
{
	return _to_ignore;
}

void ParsedFile::compute_includes(const std::map<std::string, ParsedFile>& tomakefiles)
{
	for(auto& file : _includes)
	{
		auto it = tomakefiles.find(file.path);

		if(it == tomakefiles.end())
		{
			err(string("try to compute_include an non existing file: \n" )
				+ "From file: "  + TERM::CYAN + "\"" + _adapted.path + "\"" + TERM::NOCOL + ":"
				+ " file " + TERM::ORANGE + "\"" + file.path + "\"" + TERM::NOCOL + " not found");
		}

		_includes_ptr.push_back((ParsedFile*)(&(it->second)));
	}
}

const std::vector<ParsedFile*>& ParsedFile::get_recursive_includes(const std::map<std::string, ParsedFile>& tomakefiles)
{
	if(_includes.size() == 0)
	{
		return _includes_ptr;
	}

	unsigned int fixed_size = _includes_ptr.size();

	for(unsigned int i = 0;i<fixed_size;i++)
	{
		auto& parsed = _includes_ptr[i];
		auto& sub_includes = parsed->get_recursive_includes(tomakefiles);

		for(auto& sub_parsed : sub_includes)
		{
			bool found = false;
			for(auto& incl_nodob : _includes_ptr)
			{
				if(incl_nodob == sub_parsed)
				{
					found = true;
					break;
				}
			}
			if(!found)
			{
				_includes_ptr.push_back(sub_parsed);
			}
		}
	}

	_includes.clear();

	return _includes_ptr;
}


const fileAdapter& ParsedFile::get_adapted()
{
	return _adapted;
}


map<string, ParsedFile>  get_unlinked_parsed_files(const vector<fileAdapter>& new_files_name)
{
	map<string, ParsedFile> tomakefiles;

	for(auto& nf : new_files_name)
	{
		if(to_make_file(nf.ext))
		{
			tomakefiles[nf.path]=ParsedFile(nf.path);
		}
	}

	#ifdef VERBOSE_DOTS
	cout<<endl;
	#endif

	return tomakefiles;
}