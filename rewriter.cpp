#include "rewriter.hpp"

using namespace std;

Rewriter::Rewriter(const std::string& file_path)
	: _file_path(file_path), _is_main(false)
{
	string adapted_file_path = MAKEMAKE_TMP_PATH+"/"+file_path+".makemake.cpp";

	ifstream file(file_path); 
	ofstream modified(adapted_file_path);

	string line;
	while(getline(file, line))
	{
		includeInfo linfo = get_included_file(line);

		if(linfo.type == NO_INCLUDE)
		{
			modified<<line<<endl;
		}
		else
		{
			if(linfo.type == FROM_ENV)
			{
				//modified<<line<<endl;
			}
			if(linfo.type == FROM_FILE)
			{
				modified<<line<<endl;
				modified<<NEW_INCLUDE_TOKEN<<" "<<linfo.file<<endl;
			}
		}
	}

	file.close();
	modified.close();

	system(("g++ -E " + adapted_file_path + " -o " + adapted_file_path + ".precomp").c_str());

	_includes = system_result("cat " + adapted_file_path + " | grep \'" + NEW_INCLUDE_TOKEN + "\\ \'");

	ifstream postfile(adapted_file_path);

	while(getline(postfile, line))
	{
		int i=0;
		bool int_catched = false;
		for(; i<int(line.size()); i++)
		{
			if(is_in_str(line, i, "int"))
			{
				int_catched = true;
				break;
			}
		}

		if(int_catched)
		{
			while(i<int(line.size()) && (line[i]==' ' || line[i]=='\t' || line[i]=='\n'))
			{
				i++;
			}


			if(is_in_str(line, i, "main"))
			{
				_is_main = true;
				break;
			}
		}
	}

	postfile.close();

}

bool Rewriter::is_main() const
{
	return _is_main;
}


extTypes Rewriter::get_ext() const
{
	return _ext;
}

const std::vector<std::string>& Rewriter::get_includes() const
{
	return _includes;
}

const std::vector<std::string>& Rewriter::get_env_includes() const
{
	return _env_includes;
}

const std::vector<flagTypes>&   Rewriter::get_flags() const
{
	return _flags;
}

const std::vector<LinkedLibrary*>& Rewriter::get_libs() const
{
	return _libs;	
}




